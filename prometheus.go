package ph_route_middleware

import (
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

type HttpMetrics struct {
	totalRequests  *prometheus.CounterVec
	responseStatus *prometheus.CounterVec
	duration       *prometheus.HistogramVec
	timer          *prometheus.Timer
}

func New() *HttpMetrics {
	var totalRequests = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "http_requests_total",
			Help: "Number of get requests.",
		},
		[]string{"path"},
	)

	var responseStatus = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "response_status",
			Help: "Status of HTTP response",
		},
		[]string{"status"},
	)

	var httpDuration = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name: "http_response_time_seconds",
		Help: "Duration of HTTP requests.",
	}, []string{"path"})
	h := &HttpMetrics{
		totalRequests:  totalRequests,
		responseStatus: responseStatus,
		duration:       httpDuration,
	}
	prometheus.Register(h.totalRequests)
	prometheus.Register(h.duration)
	prometheus.Register(h.responseStatus)
	return h
}

func (m *HttpMetrics) TotalRequestInc(url string) {
	m.totalRequests.WithLabelValues(url).Inc()
}

func (m *HttpMetrics) ReqTimerStart(url string) {
	m.timer = prometheus.NewTimer(m.duration.WithLabelValues(url))
}

func (m *HttpMetrics) ReqTimerStop() {
	m.timer.ObserveDuration()
}
