package ph_route_middleware

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"strings"
)

type IDToken struct {
	Sub                 string   `json:"sub"`
	CognitoGroups       []string `json:"cognito:groups"`
	EmailVerified       bool     `json:"email_verified"`
	Gender              string   `json:"gender"`
	Iss                 string   `json:"iss"`
	PhoneNumberVerified bool     `json:"phone_number_verified"`
	CognitoUsername     string   `json:"cognito:username"`
	CustomUserUUID      string   `json:"custom:user-uuid"`
	CustomGroupUUID     string   `json:"custom:group-uuid"`
	GivenName           string   `json:"given_name"`
	Aud                 string   `json:"aud"`
	EventID             string   `json:"event_id"`
	TokenUse            string   `json:"token_use"`
	AuthTime            int      `json:"auth_time"`
	PhoneNumber         string   `json:"phone_number"`
	Exp                 int      `json:"exp"`
	Iat                 int      `json:"iat"`
	FamilyName          string   `json:"family_name"`
	Email               string   `json:"email"`
}

type Jwt struct {
	Sub           string   `json:"sub"`
	CognitoGroups []string `json:"cognito:groups"`
	EventID       string   `json:"event_id"`
	TokenUse      string   `json:"token_use"`
	Scope         string   `json:"scope"`
	AuthTime      int      `json:"auth_time"`
	Iss           string   `json:"iss"`
	Exp           int      `json:"exp"`
	Iat           int      `json:"iat"`
	Jti           string   `json:"jti"`
	ClientID      string   `json:"client_id"`
	Username      string   `json:"username"`
}

type Permissions struct {
	allowedGroups []string
}

func NewPermissions(grpArgs ...string) *Permissions {
	var grps []string
	for i := 0; i < len(grpArgs); i++ {
		grps = append(grps, strings.ToLower(grpArgs[i]))
	}
	return &Permissions{allowedGroups: grps}
}

func (p *Permissions) checkPermissions(ctx context.Context) error {
	rawJwt, ok := ctx.Value("jwt").(string)
	if !ok {
		return fmt.Errorf("could not get jwt from context")
	}
	jwt, err := unmarshalIJwt(rawJwt)
	if err != nil {
		return fmt.Errorf("failed to unmarshallJwt")
	}
	// if this evaluates to true it means the user is a service using oauth
	// and group membership evaluation should be skipped
	if !strings.Contains(jwt.Scope, "rs/rs.write") {
		rawIdToken, ok := ctx.Value("idToken").(string)
		if !ok {
			return fmt.Errorf("could not get idtoken from context")
		}
		if rawIdToken == "" {
			return fmt.Errorf("could not get idtoken is empty")
		}
		grpMember := false
		idToken, err := unmarshalIDToken(rawIdToken)
		if err != nil {
			return fmt.Errorf("could not decode identity")
		}
		for j := 0; j < len(p.allowedGroups); j++ {
			for i := 0; i < len(idToken.CognitoGroups); i++ {
				if strings.Contains(strings.ToLower(idToken.CognitoGroups[i]), p.allowedGroups[j]) {
					grpMember = true
				}
			}
		}
		if !grpMember {
			return fmt.Errorf("you do not have the correct permissions to access this resource")
		}
	}
	return nil
}

func unmarshalIDToken(token string) (*IDToken, error) {
	userIDRaw := strings.Split(token, ".")
	decoded, err := base64.RawURLEncoding.DecodeString(userIDRaw[1])
	if err != nil {
		return nil, err
	}
	idToken := &IDToken{}
	err = json.Unmarshal(decoded, idToken)
	if err != nil {
		return nil, err
	}
	return idToken, nil
}

func unmarshalIJwt(token string) (*Jwt, error) {
	jwtRaw := strings.Split(token, ".")
	decoded, err := base64.RawURLEncoding.DecodeString(jwtRaw[1])
	if err != nil {
		return nil, err
	}
	jwt := &Jwt{}
	err = json.Unmarshal(decoded, jwt)
	if err != nil {
		return nil, err
	}
	return jwt, nil
}
