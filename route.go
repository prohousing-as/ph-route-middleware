package ph_route_middleware

import (
	"encoding/json"
	"net/http"
)

type phRouteHandler struct {
	permissions *Permissions
	metrics     *HttpMetrics
}

func NewPhRouteHandler(p *Permissions, m *HttpMetrics) *phRouteHandler {
	return &phRouteHandler{
		permissions: p,
		metrics:     m,
	}
}

func (p *phRouteHandler) HandleRoute(h http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		ctx := r.Context()
		p.metrics.TotalRequestInc(r.URL.String())
		p.metrics.ReqTimerStart(r.URL.String())
		err := p.permissions.checkPermissions(ctx)
		if err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			_ = json.NewEncoder(w).Encode(map[string]string{"error": err.Error()})
			return
		}
		h.ServeHTTP(w, r)
		p.metrics.ReqTimerStop()
	}
}
